document.addEventListener("scroll", function (e) {
  var nav = document.querySelector("nav");
  nav.classList.toggle("scrolled", window.scrollY > nav.scrollHeight);
});
